package br.com.calculos;

public class Maffetone {

	private int maffetone = 180;
	
	private int casoA = 10;
	private int casoB = 5;
	
	
	public boolean recebeIdade(int idade){
		
		int idadeRecebida = idade;
		
		if(idadeRecebida < 0 || idadeRecebida > 120){
			return false;
		}else{
			return true;
		}
	}
	
	
	public int[] calculcarMaffetone(int idadeRecebida){
		
		if(recebeIdade(idadeRecebida)){
			return new int[]{maffetone - idadeRecebida - 10,maffetone - idadeRecebida};
		}else{
			return new int[]{0,0};
		}
	}
	
	public int[] calcularCasoA(int idadeRecebida){
		
		return new int[]{(calculcarMaffetone(idadeRecebida)[0] - casoA),calculcarMaffetone(idadeRecebida)[1] - casoA};

	}
	
public int[] calcularCasoB(int idadeRecebida){
		
		return new int[]{calculcarMaffetone(idadeRecebida)[0] - casoB,calculcarMaffetone(idadeRecebida)[1] - casoB};

	}
	
	
}
