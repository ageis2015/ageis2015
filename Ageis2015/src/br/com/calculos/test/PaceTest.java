package br.com.calculos.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.com.calculos.Maffetone;
import br.com.calculos.Pace;

public class PaceTest {
	Pace pace;
    @Before public void initialize() {
    	pace = new Pace();
	}

	@Test
	public void testeHora1Minuto1Segundo1() {
		assertEquals((Double)3661.0, (Double)pace.recebeTempo(1, 1, 1));
	}
	
	@Test
	public void teste20Kilometros() {
		assertEquals((Double)20.0, (Double)pace.recebeDistancia(20));
	}
	
	@Test
	public void testePace() {
		assertEquals((Double)183.05, (Double)pace.calculaPaceEmSegundos(pace.recebeTempo(1, 1, 1), pace.recebeDistancia(20)));
	}
	
	@Test
	public void testePaceFinal() {
		assertArrayEquals(new int[]{0,3,3}, pace.retornaTempoPace((Double)pace.calculaPaceEmSegundos(pace.recebeTempo(1, 1, 1), pace.recebeDistancia(20))));
	}
}
