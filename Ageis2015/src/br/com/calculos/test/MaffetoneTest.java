package br.com.calculos.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import br.com.calculos.Maffetone;

public class MaffetoneTest {

	Maffetone maffetone;
    @Before public void initialize() {
    	maffetone = new Maffetone();
	}

	@Test
	public void testeIdade1() {
		assertEquals(true, maffetone.recebeIdade(20));
	}

	@Test
	public void testeIdadeNegativa() {
		assertEquals(false, maffetone.recebeIdade(-1));
	}

	@Test
	public void testeIdadeMaior121() {
		assertEquals(false, maffetone.recebeIdade(121));
	}

	@Test
	public void testeMaffetone160() {
		assertArrayEquals(new int[]{150,160}, maffetone.calculcarMaffetone(20));
	}

	@Test
	public void testeMaffetone135() {
		assertArrayEquals(new int[]{125,135}, maffetone.calculcarMaffetone(45));
	}

	@Test
	public void testeMaffetoneCasoA135() {
		assertArrayEquals(new int[]{125,135}, maffetone.calcularCasoA(35));
	}

	@Test
	public void testeMaffetoneCasoA115() {
		assertArrayEquals(new int[]{105,115}, maffetone.calcularCasoA(55));
	}

	@Test
	public void testeMaffetoneCasoB135() {
		assertArrayEquals(new int[]{125,135}, maffetone.calcularCasoB(40));
	}

	@Test
	public void testeMaffetoneCasoB115() {
		assertArrayEquals(new int[]{105,115}, maffetone.calcularCasoB(60));
	}

}
