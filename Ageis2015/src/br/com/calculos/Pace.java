package br.com.calculos;

public class Pace {
	//Tempo usado para calcular o pace
	double tempoEmSegundos = 0.0;
	
	//Dados da distancia
	int distanciaKM;

	public double recebeTempo(int hora, int minuto, int segundo)
	{
		tempoEmSegundos += hora * 3600;
		tempoEmSegundos += minuto * 60;
		tempoEmSegundos += segundo;
		
		return tempoEmSegundos;
	}
	
	public double recebeDistancia(int distancia)
	{
		distanciaKM = distancia;
		return distanciaKM;
	}
	
	public double calculaPaceEmSegundos(double tempoSegundos, double distanciaKM)
	{
		double paceEmSegundos;
		
		paceEmSegundos = tempoSegundos / distanciaKM;
		
		return paceEmSegundos;
	}
	
	public int[] retornaTempoPace(double paceSegundos)
	{

	    int horas = (int) paceSegundos / 3600;
	    int restante = (int) paceSegundos - horas * 3600;
	    int minutos = restante / 60;
	    restante = restante - minutos * 60;
	    int segundos = restante;

	    int[] tempoBonito = {horas , minutos , segundos};
	    return tempoBonito;
	    
	    
	}
}
